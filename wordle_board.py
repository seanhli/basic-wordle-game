# For Wordle.py use
# Class with ability to create a dynamic sized board
#   and update its state

import string
from random import randint
from word_generator import word_generator
from collections import defaultdict

# Thank you Peter Mortensen and joeld
# https://stackoverflow.com/questions/287871/how-do-i-print-colored-text-to-the-terminal

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKYELLOW = '\033[33m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


#create a board to display guessed letters and guesses
class game_board:

    def __init__(self, word: str, guess_max: int):
        self.word = word
        self.guess_max = guess_max
        self.guess_count = 0
        self.guess_history = []
        self.unused_letters = list(string.ascii_lowercase[:26])
        self.victory = False
        self.board = defaultdict(list)
        self.generator = word_generator()

    def create_new_board(self):

        #board layout
        # a1 | a2 | a3 | a4 | a5 ...
        # -----------------------
        # b1 | b2 | b3 | b4 | b5 ...
        # ...

        vert_sep = "|"
        hor_sep = "---"

        #create rows
        for i in range(self.guess_max*2-1):
            #reset aspects
            self.guess_count = 0
            self.guess_history = []
            self.unused_letters = list(string.ascii_lowercase[:26])
            self.victory = False

            counter = 1

            #only for even rows
            if i % 2 == 0:
                for letter in self.word:
                    self.board[i].append([" ",0])
                    if counter < len(self.word):
                        self.board[i].append([vert_sep,0])
                    counter += 1
            #only for odd rows
            else:
                for letter in self.word:
                    self.board[i].append([hor_sep,0])
                    if counter < len(self.word):
                        self.board[i].append([hor_sep,0])
                    counter += 1

    def show_board(self):
        print()
        for k, v in self.board.items():
            for item in v:
                if k % 2 == 0:
                    print((bcolors.OKGREEN if item[1] == 2 else bcolors.OKYELLOW if item[1] == 1 else bcolors.ENDC) + f' {str(item[0])} ' + bcolors.ENDC, end = "")
                else:
                    print(str(item[0]), end = "")
            print()
        print()
        print("Unused letters:")
        print(self.unused_letters)
        print()


    def update_board(self):
        if self.guess_count > 0:
            for i in range(0,self.guess_count*2-1,2):
                for k in range(0,len(self.word)*2-1,2):
                    #only even values within values of board need to be replaced
                    self.board[i][k] = self.guess_history[int(i/2)][int(k/2)]

        self.show_board()


    def make_guess(self):
        guess_detail = []
        self.guess_count += 1
        valid_word = False

        while valid_word == False:
            guess = ""
            while len(guess) != len(self.word):
                guess = input("Please enter a word: ")
                if len(guess) != len(self.word):
                    print(bcolors.WARNING + "Incorrect length. Please try again" + bcolors.ENDC)
                    print()
            valid_word = self.generator.check_word(guess)
            if not valid_word:
                print(bcolors.WARNING + "That is not a valid word. Please try again" + bcolors.ENDC)

        self.victory = True if guess == self.word else False

        #check correct letters
        for index in range(len(guess)):
            #Guess state contains
            #[0]: guessed letter
            #[1]: integer, 2 if in correct position, 1 if in word, 0 if not in word
            guess_state = [guess[index],0]
            if guess[index] == self.word[index]:
                guess_state[1] = 2
            elif guess[index] in self.word:
                guess_state[1] = 1
            if guess[index] in self.unused_letters:
                self.unused_letters.remove(guess[index])

            guess_detail.append(guess_state)

        self.guess_history.append(guess_detail)
