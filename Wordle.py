# Wordle game
# Created by Sean Li
# Created on Aug 19, 2022
# Last updated on Aug 22, 2022
# version 1.2

from word_generator import word_generator
from wordle_board import game_board
from random import randint

def play_wordle():
    #default settings

    print("Welcome to my jank version of Wordle!")
    print("Let's set some parameters first")
    print("Letter count: type a number between 4 and 9, or anything else for a random #")
    temp = input()

    if temp.isnumeric() and int(temp) >= 4 and int(temp) <= 9:
        word_length = int(temp)
    else:
        word_length = randint(4,9)

    print("How many guesses do you want? (max 10)")
    temp = input()

    if temp.isnumeric() and int(temp) > 0 and int(temp) <= 10:
        guesses_allowed = int(temp)
    else:
        guesses_allowed = 6

    print("Do you want to allow duplicate letters? Y/N")
    temp = input().lower()

    if temp == "y":
        dedupe = False
    else:
        dedupe = True

    print()
    print(f'Lets get started! You have {guesses_allowed} tries')
    print()

    #create word and board
    generator = word_generator()
    word_check = generator.generate_random_word(word_length, dedupe)
    board = game_board(word_check, guesses_allowed)
    board.create_new_board()
    board.show_board()

    while board.victory == False and board.guess_count < board.guess_max:
        board.make_guess()
        board.update_board()

    if board.victory == False:
        print(f'The word was {word_check}. Better luck next time!')
    else:
        print(f'Congratulations! You succeeded in {board.guess_count} tries!')

play_wordle()
