Wordle game
By Sean Li

Made as a practice project for Hack Reactor to practice functions, classes, using list + dictionary data structures, and using API calls

Update notes:
v0.1: updated on 8/19
- Added a random word generator that generates a word from MIT's word list of 100,000 and cross references it against Free Dictionary to validate the word
- Added dedupe functionality to generate words with no repeating letters

v1.0: updated on 8/20
- Implemented core structure and logic of Wordle game
- Added visualization of Wordle game state
- Game is now playable

v1.1: updated on 8/21
- Added a list view of unused letters to assist player
- Implemented a valid word check for user input

v1.2: updated on 8/22
- Organized code for classes on be on separate files

For future updates:
- Emulate Wordle's challenge difficulty where users must use letters they've uncovered and in their respective positions
- Add a game history and streak counter
